import cv2
import sqlite3
import os

conn = sqlite3.connect('test.db')
c = conn.cursor()
fname = "recognizer/trainner.yml"
if not os.path.isfile(fname):
    print("please train the data first")
    exit(0)
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read(fname)
face_cascade = cv2.CascadeClassifier('C:\\Users\\ajay5\\Anaconda3\\Lib\\site-packages\\cv2\\data\\haarcascade_frontalface_default.xml')
cap = cv2.VideoCapture(0)
fontFace = cv2.FONT_HERSHEY_SIMPLEX
fontScale = 1
fontColor = (0, 255, 0)

while True:
    ret, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 6)
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
        ids, conf = recognizer.predict(gray[y:y + h, x:x + w])
        print(ids)
        if (ids == 1):
            ids = "Sai hareesha"
            print(ids)
            sql_command = """update att set status="present" where name="Sai hareesha";"""
            c.execute(sql_command)
            conn.commit()
            cv2.putText(img, str(ids), (x, y), fontFace, fontScale, fontColor)
        if (ids == 2):
            ids = "Shravya"
            sql_command = """update att set status="present" where name="Shravya";"""
            c.execute(sql_command)

            conn.commit()
            cv2.putText(img, str(ids), (x, y), fontFace, fontScale, fontColor)

        if (ids == 3):
            ids = "Ramya"
            sql_command = """update att set status="present" where name="Ramya";"""
            c.execute(sql_command)
            conn.commit()
            cv2.putText(img, str(ids), (x, y), fontFace, fontScale, fontColor)
        if (ids == 4):
            ids = "Deepak"
            sql_command = """update att set status="present" where name="Deepak";"""
            c.execute(sql_command)
            conn.commit()
            cv2.putText(img, str(ids), (x, y), fontFace, fontScale, fontColor)

    cv2.imshow('FaceRecognizer', img)
    if cv2.waitKey(200) & 0xFF == ord('q'):
        break
conn.commit()
conn.close()
cap.release()
cv2.destroyAllWindows()

