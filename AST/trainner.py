import cv2, os
import numpy as np
from PIL import Image

recognizer = cv2.face.LBPHFaceRecognizer_create()
path = 'dataset'

def getImagesAndLabels(path):
    imagePaths = []
    for f in os.listdir(path):
        imagePaths.append(os.path.join(path, f))
    facessamples = []
    Ids = []
    for i in imagePaths:
        pilImage = Image.open(i)
        # pilImage=cv2.imread(i,0)
        imageNp = np.array(pilImage, 'uint8')
        Id = int(os.path.split(i)[-1].split(".")[1])
        facessamples.append(imageNp)
        Ids.append(Id)
        print(Id)
        cv2.imshow("trainner", imageNp)
        cv2.waitKey(100)
    return Ids, facessamples


Ids, faces = getImagesAndLabels(path)
recognizer.train(faces, np.array(Ids))
print("success")
recognizer.write("recognizer/trainner.yml")
cv2.destroyAllWindows()

