import cv2

cam = cv2.VideoCapture(0)
detector = cv2.CascadeClassifier('C:\\Users\\ajay5\\Anaconda3\\Lib\\site-packages\\cv2\\data\\haarcascade_frontalface_default.xml')
Id = input('enter your id')
sampleNum = 0
while (True):
    ret, img = cam.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces = detector.detectMultiScale(gray, 1.3, 5)
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
        sampleNum = sampleNum + 1
        cv2.imwrite("dataset/User." + Id + '.' + str(sampleNum) + ".jpg", gray[y:y + h, x:x + w])

        cv2.imshow('Face Recognizer', img)
    if cv2.waitKey(20) & 0xFF == ord('a'):
        break
    elif sampleNum > 30:
        break
print("relese")
cam.release()
cv2.destroyAllWindows()
